<!DOCTYPE html>
<html lang="th" class="isDesktop">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Database</title>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Sarabun&display=swap" rel="stylesheet">
  <style>
    .body {
      font-family: 'Sarabun', sans-serif;
      margin: 0;
      padding: 0;
    }

    .title-head {
      padding: 3px;
      text-align: justify;
      color: #134D8F;
    }

    .button {
      max-width: 100%;;
      margin: 25px auto;
      text-align: center;
    }

    .button a {
      font-family: 'Sarabun';
      font-size: 16px;
    }

    .outline-button {
      display: inline-block;
      padding: 10px 20px;
      border: 2px solid #ffffff;
      color: #134D8F;
      border-radius: 15px;
      cursor: pointer;
      font-size: 16px;
      transition: background-color 0.3s, color 0.3s;
      text-decoration: none;
    }

    .outline-button:hover {
      background-color: #134D8F;
      color: #fff;
    }
  </style>
</head>
<body>
<div class="title-head">
    <h1>Connected Database Server<h1>
        <h1>Selected database</h1>
  </div>
<?php
   $servername = "db";
   $username = "devops";
   $password = "devops101";

   $dbhandle = mysqli_connect($servername, $username, $password);
   $selected = mysqli_select_db($dbhandle, "titanic");
   
   echo "Connected database server<br>";
   echo "Selected database";
?>
  <div class="button">
    <a href="index.html" class="outline-button">Profile</a>
    <a href="interested.html" class="outline-button">สิ่งที่สนใจ</a>
    <a href="about_su.html" class="outline-button">รอบรั้วศิลปากร</a>
  </div>
</body>
</html>
